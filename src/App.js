import logo from "./logo.svg";
import "./App.css";

import { useState } from "react";

function App() {
  const [number, setNumber] = useState(Math.floor(Math.random() * 100) + 1);

  function randomNumber() {
    setNumber(Math.floor(Math.random() * 100) + 1);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <span>{number}</span>
        <button onClick={randomNumber}>Gerar</button>
      </header>
    </div>
  );
}

export default App;
